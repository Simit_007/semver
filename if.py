#!/usr/bin/env python3
import sys
import json
import traceback

import constants

# label_to_branch_mapping = '{"major": "release", "minor": "feature"}'

def find_label_based_on_branch_initial_name(label_to_branch_mapping, commit_branch):
    label_based_on_branch_initial_name = None


    # if 'major' in label_to_branch_mapping and 'minor' in label_to_branch_mapping:
    #     if commit_branch.startswith('major'):
    #         label_based_on_branch_initial_name = 'major'
    #     elif commit_branch.startswith('minor'):
    #         label_based_on_branch_initial_name = 'minor'
    # elif 'major' in label_to_branch_mapping and 'minor' not in label_to_branch_mapping:
    #     if commit_branch.startswith('major'):
    #         label_based_on_branch_initial_name = 'major'
    # elif 'major' not in label_to_branch_mapping and 'minor' in label_to_branch_mapping:
    #     if commit_branch.startswith('minor'):
    #         label_based_on_branch_initial_name = 'minor'

    if 'major' in label_to_branch_mapping:
        print('major key found in label_to_branch_mapping obj')
        if commit_branch.startswith(label_to_branch_mapping['major']):
            print('branch name starts with release')
            label_based_on_branch_initial_name = 'major'
    if 'minor' in label_to_branch_mapping:
        print('minor key found in label_to_branch_mapping obj')
        if commit_branch.startswith(label_to_branch_mapping['minor']):
            print('branch name starts with feature')
            label_based_on_branch_initial_name = 'minor'
    
    
    print(f"label_based_on_branch_initial_name: {label_based_on_branch_initial_name}")
    return label_based_on_branch_initial_name



def main():


    try:
        # label_to_branch_mapping = '{"major": "release", "minor": "feature"}'
        label_to_branch_mapping = '{"major":"release"}'
        label_to_branch_mapping = json.loads(label_to_branch_mapping)

        commit_branch = 'feature/my_branch'
        label_based_on_branch_initial_name = find_label_based_on_branch_initial_name(label_to_branch_mapping, commit_branch)


        if label_based_on_branch_initial_name is not None:
            print('Its not None')
        else:
            print('Its None')
    # except Exception as ex:
    #     print("Oops!! An exception occurred while decoding json")
    #     traceback.print_exc()

    except RuntimeError as re:
        print('Oops!! An exception occurred while semver tagging!')
        print(re)

    

if __name__ == "__main__":
    sys.exit(main())
