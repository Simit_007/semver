#!/usr/bin/env python3
import os
import sys
import requests
import json

import constants


def main():

    if 'CI_MERGE_REQUEST_LABELS' in os.environ:
        print('entered if')
        mr_labels = os.environ["CI_MERGE_REQUEST_LABELS"]
        print('mr_labels', mr_labels)
        if 'version::major' in mr_labels:
            print('major version available')
        elif 'version::minor' in mr_labels:
            print('minor version available')
        else:
            print('empty')
    else:
        print('entered else')
    # a_list = os.environ["branch_initial_name"]
    # a_list = json.loads(a_list)
    # print('a_list', a_list['label_major'])
    # project_id = os.environ["CI_PROJECT_ID"]
    # print('project_id', project_id)
    # merge_request_iid = os.environ["CI_MERGE_REQUEST_IID"]
    # print('merge_request_iid', merge_request_iid)
    # if merge_request_iid != '$CI_MERGE_REQUEST_IID':


    #     endpoint = f"{constants.BASE_ENDPOINT}/projects/{project_id}/merge_requests/{merge_request_iid}?labels=version::minor"
    #     print(f'Pushing the commit to the remote repository, endpoint is: {endpoint}')
    #     mr_response = requests.put(endpoint, headers = {"PRIVATE-TOKEN": os.environ.get(constants.CI_PRIVATE_TOKEN)})
    #     print('mr_response', mr_response.json())
    # else:
    #     print('merge_request_iid is blank')


if __name__ == "__main__":
    sys.exit(main())