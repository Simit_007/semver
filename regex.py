#!/usr/bin/env python3
import os
import re
import sys
import semver
import subprocess
import requests
import re

import constants

recent_tag_without_prefix = '1.2.3-g12345678'

def is_tag_pointing_to_recent_commit(recent_tag_without_prefix):
    res = re.search(".*?-g[0-9a-z]{7,}$", recent_tag_without_prefix)
    return res

if not is_tag_pointing_to_recent_commit(recent_tag_without_prefix):
    print(f'Skipping version bumping as the most recent tag: {recent_tag_without_prefix} is already pointing to the recent commit')
else:
    print('continuing semver tagging')
