#!/usr/bin/env python3
import os
import re
import sys
import semver
import subprocess
import requests
import re

def get_commits_since_recent_tag(recent_tag_with_prefix):
    recent_tag_with_prefix = git("log", "--first-parent", "--pretty=%h", f"{recent_tag_with_prefix}..HEAD").decode().strip()
    print('recent_tag_with_prefix', recent_tag_with_prefix)
    return recent_tag_with_prefix

def git(*args):
    
    return subprocess.check_output(["git"] + list(args))

def main():

    try:
        version = "v17.0.0"
        # git describe --tags --match "v[0-9]*" --abbrev=0
        commits_since_recent_tag = get_commits_since_recent_tag(version)
        commits_lines = commits_since_recent_tag.splitlines()
        commits_lines.reverse()
        print(f"commits_lines_reversed: {commits_lines}")
        if commits_lines:
            for line in commits_lines:
                commit_sha_split = line.split("-")
                commit_sha = (commit_sha_split[0]).strip()

                print("commit_sha", commit_sha)

        print("type", type(commits_lines))

    except RuntimeError as re:
        print('Oops!! An exception occurred while semver tagging!')
        print(re)

    

if __name__ == "__main__":
    sys.exit(main())

