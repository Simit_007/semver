import argparse
import sys
import os

# def main(username="", password="", teamcity_serviceUrl="abc", build_id="xyz", properties = []):

def main(searchDir):

    for dirpath, dirnames, filenames in os.walk(searchDir):

        print('dirpath', dirpath)
        print('dirnames', dirnames)
        print('filenames', filenames)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Trigger a build')
    parser.add_argument('--sd', dest='searchDir', required = True)
    args = parser.parse_args()
    main(args.searchDir)
