import sys
import subprocess
import re
import json




def extract_mr():
    try:
        res = [{'label': 'Sim'}]
        my_label = res[0]['label']
        return my_label
    except RuntimeError as re:
        print('re 1')
        print(re)

def get_recent_tag():
    # Get the most recent tag reachable from a commit
    recent_tag = git("tag", "--sort=-creatordate").decode().strip()
    print('most recent tag reachable from a commit is:', recent_tag)
    return recent_tag

def git(*args):
    return subprocess.check_output(["git"] + list(args))


def is_tag_semver(tag):

    """
    The function checks if the tag follows the semver scheme or not

    :param prefix: The recent tag without the specified prefix
    :return: Returns True if the tag follows semver scheme, otherwise returns False
    :rtype:  Returns match object if the search is successful or None otherwise
    
    """

    res = re.search("^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$", tag)
    print('res', res)
    return res


def is_blank(num):
    if num > 10:
         status = 'crorepati'
    else:
        status = None
    return status


def find_mr_label_to_be_assigned(labels_already_assigned_to_mr):
    mr_label_to_be_assigned = None
    # if labels_already_assigned_to_mr is None or 'major' not in labels_already_assigned_to_mr and 'minor' not in labels_already_assigned_to_mr:
    if 'major' not in labels_already_assigned_to_mr and 'minor' not in labels_already_assigned_to_mr:
        print('No minor/major version found')
    elif 'major' in labels_already_assigned_to_mr:
        print('major version found')
    elif 'minor' in labels_already_assigned_to_mr:
        print('minor version found')
    



def main():

    try:
        labels = ['sim', 'nim', 'djmajor']
        find_mr_label_to_be_assigned(labels)

        # a = '{"branch_initial_major_label": "release", "branch_initial_minor_label": "feature"}'
        # a = json.loads(a)
        # print(a['branch_initial_minor_label'])

    except RuntimeError as re:
        print('re 2')
        print(re)




if __name__ == "__main__":
    sys.exit(main())

