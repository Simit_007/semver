#!/usr/bin/env python3
import os
import re
import sys
import semver
import subprocess
import requests
import re

import constants



def tests():

    # ********Scenarios for recent_tag***********
    recent_tag = ''
    recent_tag = 'v6.0.0-2-g656ec70'
    recent_tag = 'v6.0.0v-2-g656ec70'
    recent_tag = '6.0.0-2-g656ec70'
    recent_tag = '6.0.0'
    recent_tag = '6-0-0'
    recent_tag = 'dddd'
    recent_tag = 'cccc

    # Cond 1: A Merge Commit is pushed 
    # Cond 2: MR doesn't have any Major/Minor label
    # Cond 3: No Tag already available in the repo
    # Cond 4: Commit is NOT tagged manually (git tag --describe will NOT contain any Tag)
    # Output: A Tag v0.0.1 is pushed as the default Tag

    # Cond 1: A Merge Commit is pushed 
    # Cond 2: MR doesn't have any major/Minor label
    # Cond 3: Commit is NOT tagged manually (git tag --describe will contain the Tag as v0.0.1-1-commitsha)
    # Output: A Tag v0.0.2 is pushed

    # Cond 1: A Merge Commit is pushed
    # Cond 2: MR has a Major/Minor label
    # Cond 3: Commit is NOT tagged manually (git tag --describe will contain the Tag as v0.0.2-1-commitsha)
    # Output: A Tag v1.0.0 is pushed (Considering MR has a Major Label)


    # Cond 1: A Merge Commit is pushed
    # Cond 2: MR doesn't have a Major/Minor label
    # Cond 3: Commit IS tagged manually (git tag --describe will contain the Tag as say v1.0.1)
    # Cond 4: Tag is Semver compliant
    # Out 1: A Tag v1.0.1 is pushed manually
    # Out 2: No additional Tag is pused by the semver code

    # Cond 1: A Merge Commit is pushed
    # Cond 2: MR doesn't have a Major/Minor label
    # Cond 3: Commit IS tagged manually (git tag --describe will contain the Tag as say AQUA-10695)
    # Cond 4: Tag is NOT Semver compliant
    # Out 1: A Tag AQUA-10695 is pushed manually
    # Out 2: No additional Tag is pused by the semver code

    # Cond 1: A Merge Commit is pushed
    # Cond 2: MR HAS a Major/Minor label
    # Cond 3: Commit is NOT tagged manually (git tag --describe will contain the Tag as say AQUA-10695)
    # Out 1: Since Tag is NOT Semver compliant so No additional Tag is pused by the semver code

    # Cond 1: A Merge Commit is pushed
    # Cond 2: MR doesn't have a Major/Minor label
    # Cond 3: Commit IS tagged manually (git tag --describe will contain the Tag as AQUA-10696)
    # Cond 4: Tag is Semver compliant
    # Out 1: A Tag say v2.0.0 is pushed manually
    # Out 2: No additional Tag is pused by the semver code

    # Cond 1: A Merge Commit is pushed
    # Cond 2: MR has a Major/Minor label
    # Cond 3: Commit is NOT tagged manually (git tag --describe will contain the Tag as v2.0.0-1-commitsha)
    # Output: A Tag v3.0.0 is pushed (Considering MR has a Major Label)




    #***********Scenarios for recent_tag_without_prefix **********
    recent_tag_without_prefix = 'Check for all the abovementioned values of recent_tag'

    #********Scenarios for commit_sha **************
    commit_sha = ''
    commit_sha = 'valid sha'
    commit_sha = 'invalid sha'

    #********Scenarios for project_id **************
    project_id = ''
    project_id = 'valid project_id'
    project_id = 'invalid project_id'


    #****** Scenarios for merge_requests_response *********
    commit_sha = ''
    commit_sha = 'sha of commit that does not have any MR corresponding to it, gives an empty array as the response'
    commit_sha = 'sha of commit that has 1 MR corresponding to it'
    commit_sha = 'sha of commit that has more than 1 MRs corresponding to it'

    #****** Scenarios for merge_request_labels **********
    merge_request_labels = 'merge_requests_response is an empty array'
    merge_request_labels = 'merge_requests_response does not contain any version:major or version::minor label'
    merge_request_labels = 'merge_requests_response contains a version:major or minor label'
    merge_request_labels = 'merge_requests_response contains a version:major and version::minor label'


def main():
    try:        
        # Get the most recent tag reachable from a commit
        recent_tag = 'v6.0.0-2-g656ec70'
        print(f'most recent tag reachable from a commit is: {recent_tag}')
        recent_tag_without_prefix = remove_prefix(recent_tag, constants.PREFIX)

    except subprocess.CalledProcessError:
        # Default to version 1.0.0 if no tags are available
        print('No tags found, hence defaulting to version 1.0.0')
        bumped_version = "1.0.0"
    else:
        # TODO:
        # Check for Semver format
        # If yes then go ahead else return 1 with a valid message: Latest tag not following semver format
        # Skip already tagged or non conformant semantic versioning scheme commits
        if '-' not in recent_tag_without_prefix:
            print(f'Skipping version bumping as the most recent commit: {recent_tag_without_prefix} is either already tagged or not conforming to the semantic versioning scheme of vMAJOR.MINOR.PATCH')
            return 1
    
        bumped_version = get_bump_version(recent_tag_without_prefix)
        print('Point 1')
        
    print('Point 2')
    print('bumped_version', bumped_version)
    print('recent_tag_without_prefix', recent_tag_without_prefix)

    return 0


def git(*args):
    return subprocess.check_output(["git"] + list(args))

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

def get_bump_version(_latest):

    commit_sha = get_latest_commit_sha()
    print(f'sha of the most recent commit is: {commit_sha}')

    merge_request_labels = extract_merge_request_labels(commit_sha)
    print(f'Merge request label for the most recent merge request is: {merge_request_label}')

    bump_version = ''
    if constants.VERSION_MAJOR in merge_request_labels:
        bump_version = semver.bump_major(_latest)
    elif constants.VERSION_MINOR in merge_request_labels:
        bump_version = semver.bump_minor(_latest)
    else:
        bump_version = semver.bump_patch(_latest)
    
    print(f'Bump Version is: {bump_version}')
    return bump_version, commit_sha


def get_latest_commit_sha():
    print('Getting the sha of the most recent commit...')
    commit_sha = git("rev-parse", "--short", "HEAD").decode().strip()
    return commit_sha


def extract_merge_request_labels(commit_sha):

    project_id = os.environ["CI_PROJECT_ID"]
    endpoint = f"{constants.BASE_ENDPOINT}/projects/{project_id}/repository/commits/{commit_sha}/merge_requests"
    print(f"Getting the list of all the merge requests corresponding to the given commit, endpoint is: {endpoint}")
    merge_requests_response = requests.get(endpoint, headers = {"PRIVATE-TOKEN": os.environ.get(constants.CI_PRIVATE_TOKEN)})
    print('Getting the most recent merge request for the given commit...')
    merge_request_labels = merge_requests_response.json()[0]['labels']
    print(f'Labels available for the most recent merge request: {merge_request_labels}')
    return merge_request_labels



if __name__ == "__main__":
    sys.exit(main())
