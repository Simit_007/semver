# #!/bin/bash

# function try()
# {
#     [[ $- = *e* ]]; SAVED_OPT_E=$?
#     set +e
# }

# function throw()
# {
#     exit $1
# }

# function catch()
# {
#     export ex_code=$?
#     (( $SAVED_OPT_E )) && set +e
#     return $ex_code
# }

# function throwErrors()
# {
#     set -e
# }

# function ignoreErrors()
# {
#     set +e
# }




# #!/bin/bash
# export NullException=100
# export AnotherException=101

# # start with a try
# try
# (   # open a subshell !!!
#     echo "do something"

#     TEST_VAR="Sim"
#     [ -z "$TEST_VAR" ] && throw $NullException

#     # echo "do something more"
#     # executeCommandThatMightFail || throw $AnotherException

#     # echo "finished"
# )
# # directly after closing the subshell you need to connect a group to the catch using ||
# catch || {
#     # now you can handle
#     case $ex_code in
#         $NullException)
#             echo "AnException was thrown"
#         ;;
#         $AnotherException)
#             echo "AnotherException was thrown"
#         ;;
#         *)
#             echo "An unexpected exception was thrown"
#             throw $ex_code # you can rethrow the "exception" causing the script to exit if not caught
#         ;;
#     esac
# }


# set -o pipefail


# set -euo pipefail
# docker tag "a" "b"

# return_code=$?
# echo "return_code: $return_code"

# # return_code=1

# if [ ${return_code} -ne 0 ]; then

#     echo "Error"
#     exit 1
#     echo "After Error"
# fi

# echo "Reached"


TEST_VAR="Sim"
MY_VAR="";
if [ -z "$TEST_VAR" ]
then
    MY_VAR="Dim";
    echo "Error"
    exit 1
fi

echo "${MY_VAR}"
echo "$TEST_VAR"