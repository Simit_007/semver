import argparse
import sys

def main(username="", password="", teamcity_serviceUrl="abc", build_id="xyz", properties = []):

    print('username', username)
    print('password', password)
    print('teamcity_serviceUrl', teamcity_serviceUrl)
    print('build_id', build_id)
    print('properties', properties)


    if username != "sim":
        sys.exit(1)
    sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Trigger a build')
    parser.add_argument('--teamcity', dest='teamcity_serviceUrl', required = True)
    parser.add_argument('--build_id', dest='build_id', required = True)
    parser.add_argument('--username', dest='username')
    parser.add_argument('--password', dest='password')
    parser.add_argument('--properties', dest='properties', nargs='*', default=[], help = 'a=b b=c')
    args = parser.parse_args()
    main(args.username, args.password, args.teamcity_serviceUrl, args.build_id, args.properties)
